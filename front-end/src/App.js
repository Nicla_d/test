import Header from './components/common/Header';
import './App.scss';
import TableComp from './components/TableComp';

function App() {
  return (
    <div className="App">
      <Header />
      <div className="container">
        <TableComp />
      </div>

    </div>
  );
}

export default App;
