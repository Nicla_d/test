import '../style/TableComp.scss';
import icon1 from '../img/icon1.svg';
import icon2 from '../img/icon2.svg';
import icon3 from '../img/icon3.svg';
import icon4 from '../img/icon4.svg';

import React, { useState, useEffect } from 'react'
import { Table } from 'react-bootstrap'

const TableComp = () => {
    //stato per i dati ricevuti
    const [data, setData] = useState([])
    //stato per il loading (solitamente utilizzato con rotellina che gira)
    const [load, setLoad] = useState(false)

    useEffect(() => {
        fetch("https://run.mocky.io/v3/8a250f4b-3d88-4d93-a6a8-1678d050f978")
            .then(res => (res.json()))
            .then(data => {
                //recupero i dati ed aggiorno lo stato
                setData(data)
                //recuperati i dati aggiorno il loading
                setLoad(true)
            })

    }, [])

    const render_list = () => {

        // controllo se il data ricevuto è pieno
        // se si stampo la lista se no stampo una stringa di avviso
        // (se non avessi inserito il loading, inizialmente si vedrebbe la stringa del data vuoto)

        if (data.length > 0) {

            return (
                data.map((cl, key) => (
                    <tr key={cl.id}>
                        <td className="col-name">{cl.username}</td>
                        <td className="col-email">{cl.userInfo.email}</td>
                        <td className="col-level">
                            <div className={"level " + cl.level.toLowerCase()}><span>{cl.level}</span></div>
                        </td>
                        <td className={cl.enabled ? "col-status status-active" : "col-status status-inactive"}>
                            <div className="status-point"></div>
                            <span>{cl.enabled ? 'Active' : 'Inactive'}</span>
                        </td>
                        <td className="col-actions">
                            <div className="icon-action" ><img src={icon1} className="img-icon-action" alt="action1" /></div>
                            <div className="icon-action" ><img src={icon2} className="img-icon-action" alt="action2" /></div>
                            <div className="icon-action" ><img src={icon3} className="img-icon-action" alt="action3" /></div>
                            <div className="icon-action" ><img src={icon4} className="img-icon-action" alt="action4" /></div>
                        </td>
                    </tr>
                ))
            )
            
        } else {
            return (
                <tr><td colSpan={5}><span>Non ci sono dati disponibili</span></td></tr>
            )
        }
    }


    return (
        <div className="container-table">
            <div className="title">
                <p>TABLE</p>
                <div className="line-grad"></div>
            </div>
            <Table responsive className='custom-table table'>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Level</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {load && render_list()}
                </tbody>
            </Table>
        </div>
    )
}

export default TableComp