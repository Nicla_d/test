import logo from '../../img/logo.svg';

const Header = () => {
    return (
        <div className="container-logo">
            <img src={logo} className="logo-img" alt="logo" />
        </div>
    )
}

export default Header